###############################################################################
# CTFd Namespace - We run everything in here to isolate it
###############################################################################
---
apiVersion: v1
kind: Namespace
metadata:
  name: ctfd
###############################################################################
# External Service - defines the outward facing ports
###############################################################################
---
apiVersion: v1
kind: Service
metadata:
  name: ctfd-web
  namespace: ctfd
  labels:
    service: ctfd
    role: web
spec:
  type: LoadBalancer
  ports:
  - protocol: TCP
    port: 443
    targetPort: 443
  selector:
    service: ctfd
    role: web
###############################################################################
# Top level Deployment
# Defines the containers which make up the CTFd system
###############################################################################
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: ctfd-web
  namespace: ctfd
  labels:
    service: ctfd
spec:
  replicas: 1
  selector:
    matchLabels:
      service: ctfd
      role: web
  template:
    metadata:
      labels:
        service: ctfd
        role: web
    spec:
      imagePullSecrets:
      - name: regcred
      automountServiceAccountToken: false
      enableServiceLinks: false
      containers:
      - name: frontend
        image: registry.gitlab.com/cybears/fall-of-cybeartron/ctfd-bsides-2019
        ports:
        - containerPort: 8000
        env:
        - name: WORKERS
          value: '32'
        - name: DATABASE_URL
          value: mysql+pymysql://ctfd:ctfd@ctfd-db/ctfd
        - name: REDIS_URL
          value: redis://ctfd-cache:6379
        - name: UPLOAD_FOLDER
          value: /var/uploads
        - name: LOG_FOLDER
          value: /var/log/CTFd
        - name: ACCESS_LOG
          value: '-'
        - name: ERROR_LOG
          value: '-'
        volumeMounts:
        - mountPath: /var/uploads
          name: ctfd-uploads
        - mountPath: /var/log/CTFd
          name: ctfd-logs
      # nginx TLS terminator
      - name: nginx
        image: pbrumblay/tls-sidecar
        ports:
        - containerPort: 443
        volumeMounts:
        - mountPath: /etc/nginx/ssl
          name: tls-secret-volume
      # We define persistent data volumes for each of the cache and db
      volumes:
      - name: tls-secret-volume
        secret:
          secretName: ctf-tls
      - name: ctfd-uploads
        persistentVolumeClaim:
          claimName: ctfd-uploads-claim
      - name: ctfd-logs
        persistentVolumeClaim:
          claimName: ctfd-logs-claim
---
###############################################################################
# Back End Deployment
# Defines the containers which make up the CTFd database and cache
###############################################################################
# Cache
###############################################################################
apiVersion: v1
kind: Service
metadata:
  name: ctfd-cache
  namespace: ctfd
  labels:
    service: ctfd
    role: cache
spec:
  type: ClusterIP
  ports:
  - protocol: TCP
    port: 6379
    targetPort: 6379
  selector:
    service: ctfd
    role: cache
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: ctfd-cache
  namespace: ctfd
  labels:
    service: ctfd
    role: cache
spec:
  replicas: 1
  selector:
    matchLabels:
      service: ctfd
      role: cache
  template:
    metadata:
      labels:
        service: ctfd
        role: cache
    spec:
      automountServiceAccountToken: false
      enableServiceLinks: false
      containers:
      # Redis provides a caching backend
      - name: cache
        image: redis:4
        volumeMounts:
        - mountPath: /data
          name: cache-data
      # We define persistent data volumes for each of the cache and db
      volumes:
      - name: cache-data
        persistentVolumeClaim:
          claimName: cache-data-claim
---
###############################################################################
# Database
###############################################################################
apiVersion: v1
kind: Service
metadata:
  name: ctfd-db
  namespace: ctfd
  labels:
    service: ctfd
    role: db
spec:
  type: ClusterIP
  ports:
  - protocol: TCP
    port: 3306
    targetPort: 3306
  selector:
    service: ctfd
    role: db
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: ctfd-db
  namespace: ctfd
  labels:
    service: ctfd
    role: db
spec:
  replicas: 1
  selector:
    matchLabels:
      service: ctfd
      role: db
  template:
    metadata:
      labels:
        service: ctfd
        role: db
    spec:
      automountServiceAccountToken: false
      enableServiceLinks: false
      containers:
      # MariaDB is used to provide the backend database
      - name: db
        image: mariadb:10.4
        env:
        - name: MYSQL_PASSWORD
          value: ctfd
        - name: MYSQL_DATABASE
          value: ctfd
        - name: MYSQL_ROOT_PASSWORD
          value: ctfd
        - name: MYSQL_USER
          value: ctfd
        args:
          - mysqld
          - --character-set-server=utf8mb4
          - --collation-server=utf8mb4_unicode_ci
          - --wait_timeout=28800
          - --log-warnings=0
        volumeMounts:
        - mountPath: /var/lib/mysql
          name: db-data
      # We define persistent data volumes for each of the cache and db
      volumes:
      - name: db-data
        persistentVolumeClaim:
          claimName: db-data-claim

###############################################################################
# Persistent volume definitions
###############################################################################
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: ctfd-uploads-claim
  namespace: ctfd
  labels:
    content: ctfd-file-upload-data
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 100Mi

---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: ctfd-logs-claim
  namespace: ctfd
  labels:
    content: ctfd-log-data
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 100Mi

---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: db-data-claim
  namespace: ctfd
  labels:
    content: ctfd-db-data
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 100Mi

---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: cache-data-claim
  namespace: ctfd
  labels:
    content: ctfd-cache-data
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 100Mi
