#!/bin/bash
SCRIPT_PATH="$(realpath -e "${BASH_SOURCE[0]}")"
SCRIPT_DIR="${SCRIPT_PATH%/*}"
BUILD_DIR="${SCRIPT_DIR}/build/"
THEME_DIR="${SCRIPT_DIR}/../extras/themes/cybears"

# Create the enlightenment scripts HTML chunk
mkdir -p "${SCRIPT_DIR}/build/templates/enlightenment/"
sed -e 's/.*<script>/<script>/' -e 's/<\/body>//' "${BUILD_DIR}/index.html" \
    > "${BUILD_DIR}/templates/enlightenment/scripts.html"

# Copy the enlightenment build assets to the cybears theme
cp -rv -t "${THEME_DIR}"                                                    \
    "${SCRIPT_DIR}/templates"                                               \
    "${BUILD_DIR}/templates" "${BUILD_DIR}/static"
