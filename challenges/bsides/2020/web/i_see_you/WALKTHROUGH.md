# i_see_you walkthrough

This is a somewhat beginner challenge to conduct a couple of common web exploits. The user is expected to bypass the login page, identify a filename creation scheme and note the lack of authentication on an image directory.

## Hints

1. First hint
<details>
<summary>Spoiler warning</summary>
Take a look at php type juggling.
</details>

2. Second hint
<details>
<summary>Spoiler warning</summary>
What other php values are there? The type reference chart is not necessarily complete. What else could be magic?
</details>

3. Third hint
<details>
<summary>Spoiler warning</summary>
You need to find a CCTV capture that isn't listed on the page. See the handout for a clue.
</details>

## Steps

<details>
<summary>Spoiler warning</summary>

Flag requires a bypass of the login page with type juggling. PHP magic hashes require leading zeroes to eval to 0.
But what is significantly less well documented is that INF == "INF" and PHP_FLOAT_MAX+1 = INF; therefore when the leading digit is non-zero we overflow the float datatype. To abuse PHP's type juggling we need to identify an input to SHA1 which results in a value in scientific notation which exceeds PHP_FLOAT_MAX.
Reference: [https://wiki.php.net/rfc/string_to_number_comparison](https://wiki.php.net/rfc/string_to_number_comparison)

Here are some examples:
TX8GXrJlbpC6UEJpCqPBGEIDOoD9BW164MCLY5VSFdtgOgpK4M5xDTyoBCX8: 1318528e02518397170892143147409142278141
m1ctSUDKS6x3fwAeea0jkt3O25jW1EhhhavuTC7FTnC1Te7FSxRz06LKPeUU: 44962913672539e8989793691026071479387578
cBYrJq43FXABS0ZdTpCHUUQwJ8BtEwX0D93RE9VM0UBNrpAPOKypPssJbKYb: 315213574945460e077524809966815156318877

The hash is read from a json config file to preserve the float data typing of the hash since the output of hash() is a string. It could have equally been defined in an include as a float.

Once logged in, the user is presented with cctv.php. On this page we require the use of the clue in the handout.

*"Something weird happened at January 24, 2019 11:58:27 AM GMT. Perhaps the security footage may show what happened?"*

When clicking save current image on the CCTV UI, the user is presented with an error message that includes what looks like a hash as the file name. The only input provided on the UI is the camera name and current time, trying MD5(camera1+unixtime) gives the exact filename from the error message. Using this scheme, MD5(camera1+unixtime(January 24, 2019, 11:58:27 AM GMT)) and browsing to the resulting hash c5e4cf4a3b5096b02f7d4435bac76160.jpg. Browsing to that image in the /cctv/ subdir displays the flag.


```
session = HTMLSession()

print("Exploiting login page")
r = session.post("http://" + args.remote + "/login.php", data={'username': 'admin', 'password': 'TX8GXrJlbpC6UEJpCqPBGEIDOoD9BW164MCLY5VSFdtgOgpK4M5xDTyoBCX8'})
loginResult = r.url
assert 'loggedin' in loginResult

print("Validating Flag image file")
r = session.get("http://" + args.remote + "/cctv/c5e4cf4a3b5096b02f7d4435bac76160.jpg")
h = hashlib.sha256(r.content).hexdigest()
assert '64c5db808b2a3c52041745b73c5c2dba917da19b57fed5ba41c65b065a01bdde' == h


```

</details>

