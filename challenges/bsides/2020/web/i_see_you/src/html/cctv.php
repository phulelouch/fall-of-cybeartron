<?php

require_once("auth.php");


if (isset($_GET['action']) && $_GET['action'] == "save") {
	echo "Error saving image to cctv/" . md5($_GET['id'] . time()) . ".jpg";
}

?>

<html>
<head>
<link rel="stylesheet" type="text/css" href="layout.css" media="screen" />
<title> Cybear Networking Systems&copy;</title>
</head>

<body>

<div class="header">
	<h1>Security Camera Monitoring</h1>
</div>

<?php
	require("header.php");
?>

<div class="row">

<div class="column">
	<img style="width: 100%" src="cctv/live.jpg" alt="Live Feed">
</div>

<div class-"column">
	<div class="infobox" style="width: 350px;">
	<h2>Status</h2>
	<img src="images/green.svg" width="16px" height="16px"> Operational 
	<br />
	<ul>
		<li>Name: camera1</li>
		<li>Last Update: <?php echo time(); ?>
		<li><a href="cctv.php?action=save&id=camera1">Save Current Image</a></li>
	</ul>
	<br />
	</div>
</div>

<div class-"column">
	<div class="infobox" style="width: 350px;">
	<h2>Saved Images</h2>
	<ul>
		<li><a href="cctv/99486372cba9b1a478f652d558877b8e.jpg">February 27, 2019 1:11:01 PM GMT</a></li>
		<li><a href="cctv/b4fc0f1a01e29b193a5829e7f8eb32dd.jpg">February 25, 2019 2:00:02 PM GMT</a></li>
		<li><a href="cctv/c81dc714ef54999798536d24c1f366a1.jpg">February 2, 2019 4:00:07 PM GMT</a></li>
		<li><a href="cctv/8c0240b18744aec93a37d8fe8bc5eb6f.jpg">December 6, 2018 3:45:07 PM GMT</a></li>
	</ul>
	</div>
</div>

</div>
<p></p>
<p></p>
<p></p>
<p></p>
<p></p>


<?php
	require("footer.php");
?>

</body>
</html>
