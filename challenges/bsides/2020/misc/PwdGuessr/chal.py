#!/usr/bin/env python3

import random
import hashlib
import datetime
from itertools import zip_longest
import pathlib
import time

SCRIPT_DIR = pathlib.Path(__file__).parent

# Load the password components
pwd_comps = []
with (SCRIPT_DIR / 'pwd_comps.txt').open('r') as c:
    for line in c:
        pwd_comps.append(line.strip().split(';'))
# Set timer values
# ===== If network is laggy, change this value ======
network = 0.005  # Make sure this value is higher than the network latency (rtt in seconds)
# ==============================================
DELAY = 100 * network
TIME_LIMIT = 150 * network + 80 * DELAY  # Connection timeout
# In the worst case, a password will require about 70 correct guesses, and about the same incorrect guesses

# To make timeout begin from the time of the first guess, put this line immediately after the first prompt
start_time = time.time()


def get_flag():
    with (SCRIPT_DIR / 'flag.txt').open('r') as f:
        return f.read().strip()


def gen_pwd():
    """
    Generate a new password according to the challenge
    :return: a string to be matched as a 'password'
    """
    pwd = ''
    for component_list in pwd_comps:
        component = random.choice(component_list)
        pwd += component
    return pwd


def gen_creds():
    # First we generate a password
    pwd = gen_pwd()
    # Then we generate a user id based off the password (and the current time, to prevent memoising)
    m = hashlib.md5()
    m.update(pwd.encode())
    m.update(str(datetime.datetime.now()).encode())
    h = m.digest()
    uid = int.from_bytes(h[:3], 'big')
    return uid, pwd


def check_pwd(sample, pwd):
    for s, p in zip_longest(sample, pwd):
        if s is None:
            # Submitted guess is too short
            return False
        if p is None:
            # Submitted guess is too long, but has the correct pwd as a prefix.
            # Do I want to treat that differently? Not at this stage
            return False
        if p != s:
            time.sleep(DELAY)
            return False
        # If we get to here, the two characters must be equal
        assert p == s
    # If we get to here, then the sample exactly matches the password
    return True


def do_chal():
    uid, pwd = gen_creds()
    print(f'Welcome, user {uid}.')
    pwd_guess = input('Please enter your password: ')
    while not check_pwd(pwd_guess, pwd):
        if time.time() - start_time > TIME_LIMIT:
            print("Connection timeout")
            return
        # Allow an unbounded number of guesses, until the time limit is up
        pwd_guess = input('Sorry, that password was incorrect. Please enter your password: ')
    # Exit out of the while loop - the password must have been right
    print(get_flag())


if __name__ == '__main__':
    do_chal()
