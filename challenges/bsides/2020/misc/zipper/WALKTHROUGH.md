# Zipper Walkthrough

* This puzzle walks players through understanding unicode encodings
* The first clue is that a zip file includes a "comments" field. This can be viewed with `unzip -l [input file]`

## Level 0
* Running `unzip -l level0.zip` gives
```bash
$ unzip -l level0.zip
Archive:  level0.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
     2389  2020-04-02 11:05   level1.zip
The password is d0hvc0FQcjNUVHlCb3lUaGVu
      636  2020-04-02 11:05   journal0.txt
---------                     -------
     3025                     2 files
```
* The password for level0.zip is `d0hvc0FQcjNUVHlCb3lUaGVu`
* `unzip -P d0hvc0FQcjNUVHlCb3lUaGVu level0.zip`


## Level 1

* Running `unzip -l level1.zip` gives

```bash
$ unzip -l level1.zip
Archive:  level1.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
     1733  2020-04-02 11:05   level2.zip
The password is not d0hvc0FQcjNUVHlCb3lUaGVu
      372  2020-04-02 11:05   journal1.txt
---------                     -------
     2105                     2 files
```

* base64 decoding the comment gives the password for level1.zip

```bash
$ echo "d0hvc0FQcjNUVHlCb3lUaGVu" | base64 -d
wHosAPr3TTyBoyThen
```
* The password for level1.zip is `wHosAPr3TTyBoyThen`
* `unzip -P "$(echo "d0hvc0FQcjNUVHlCb3lUaGVu" | base64 -d)" level1.zip`

## Level 2
* Running `unzip -l level2.zip` gives
```bash
$ unzip -l level2.zip
Archive:  level2.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
     1120  2020-04-02 11:05   level3.zip
d9Cd0L5z0JDQoHLQl9Ci0KLRg9CSb9GD0KJo0LVu
      284  2020-04-02 11:05   journal2.txt
---------                     -------
     1404                     2 files
```

* base64 decoding the comment gives...
```bash
$ echo "d9Cd0L5z0JDQoHLQl9Ci0KLRg9CSb9GD0KJo0LVu" | base64 -d
wНоsАРrЗТТуВoуТhеn
```

* This looks to be the same as level2.zip, although the base64 is different (it's longer)
* Copy and pasting the output decrypts level3.zip
* This is somewhat surprising, but starts to hint at the core of this challenge
* The password looks the same, but is actually a different unicode encoding with "homoglyphs", symbols which look like english letters, but are not.
* You can see this by looking at the hexdump:
```bash
$ echo "d9Cd0L5z0JDQoHLQl9Ci0KLRg9CSb9GD0KJo0LVu" | base64 -d | hexdump -C
00000000  77 d0 9d d0 be 73 d0 90  d0 a0 72 d0 97 d0 a2 d0  |w....s....r.....|
00000010  a2 d1 83 d0 92 6f d1 83  d0 a2 68 d0 b5 6e        |.....o....h..n|
0000001e

```
* You can also detect the encoding used with the [Cyberchef](https://gchq.github.io/CyberChef/) "Data Format/Text Encoding Bruteforce" recipe
* `unzip -P "$(echo "d9Cd0L5z0JDQoHLQl9Ci0KLRg9CSb9GD0KJo0LVu" | base64 -d)" level2.zip`


## Level 3
* The comment for level3.zip is
```bash
$ unzip -l level3.zip
Archive:  level3.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      554  2020-04-02 11:05   level4.zip
Back-to-front(АNТІRОМАN)
      228  2020-04-02 11:05   journal3.txt
---------                     -------
      782                     2 files
```
Looking at the comment in hexdump shows that many of the characters are non-ascii.
Again, using cyberchef, we can see that the encoding is Cyrillic
(Windows-1251). The wikipedia page for the Cyrillic codepage allows us to find
the backwards N and R letters we need. Finally, the password is `ИАМОЯІТИА`

```bash
$ unzip -P "ИАМОЯІТИА" level3.zip`
# OR if you don't have a unicode terminal you could do
$ unzip -P "$(python3 -c "print(b'\xd0\x98\xd0\x90\xd0\x9c\xd0\x9e\xd0\xaf\xd0\x86\xd0\xa2\xd0\x98\xd0\x90'.decode('utf-8'))")" level3.zip
```

## Level 4
* Looking at the comment again:
```
$ echo "0KHRk9Cg0pHQoNGR0KDQhtCg0ZHQoeKAmtCgwrXQoMK70KHQitCg0IXQoeKAudCg4oSW0KDRl9CgwrDQodCC0KDRldCgwrvQodCKMTIz" | base64 -d
СѓРґРёРІРёС‚РµР»СЊРЅС‹Р№РїР°СЂРѕР»СЊ123
```
* This doesn't particularly help, mainly because my terminal is not set to the correct encoding to display.
* This is an example of mojibake, or krakozyabry, where a terminal or browser renders the incorrect encoding.
* Using Cyberchef with a "Base64 decode" and a "Encode Text: Windows-1251 Cyrillic (1251)" [recipe][1] returns:
```
удивительныйпароль123
```
* This is the password for level4.zip (and google translates to "amazingpassword123" in Russian!)
* Extracting level4.zip returns the text document containing the flag.
```bash
$ unzip -P "удивительныйпароль123" level4.zip
# OR
$ unzip -P "$(python3 -c "print(b'\xd1\x83\xd0\xb4\xd0\xb8\xd0\xb2\xd0\xb8\xd1\x82\xd0\xb5\xd0\xbb\xd1\x8c\xd0\xbd\xd1\x8b\xd0\xb9\xd0\xbf\xd0\xb0\xd1\x80\xd0\xbe\xd0\xbb\xd1\x8c123'.decode('utf-8'))")" level4.zip
```

[1]: https://gchq.github.io/CyberChef/#recipe=From_Base64('A-Za-z0-9%2B/%3D',true)Encode_text('Windows-1251%20Cyrillic%20(1251)')&input=MEtIUms5Q2cwcEhRb05HUjBLRFFodENnMFpIUW9lS0FtdENnd3JYUW9NSzcwS0hRaXRDZzBJWFFvZUtBdWRDZzRvU1cwS0RSbDlDZ3dyRFFvZENDMEtEUmxkQ2d3cnZRb2RDS01USXo
