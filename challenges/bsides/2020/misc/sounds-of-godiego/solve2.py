# requires python2
# pip install python-midi
import os.path
import midi

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))

t = midi.read_midifile(os.path.join(SCRIPT_DIR, "./R29kaWVnbw==.mid"))

#track 0 is the only one with
# 1. ProgramChangeEvents (the others only have NoteOn/NoteOff events)
# 2. and tick=0, channel=0 and,
# 3. the only track where the data is an array of 1 (other tracks always have an array of 2 in the NoteOn/NoteOff data)
# 4. the data is always in a printable ascii range
track0 = t[0]

data = []
for m in track0: #extract the data from ProgramChangeEvents in track 0
    if m.is_event(midi.ProgramChangeEvent.statusmsg):
        data.append(m.data[0])

data.reverse() #visually inspecting data shows that the flag is printed in reverse order
print("FLAG is {}".format("".join(map(chr,data))))
