#include <jni.h>
#include <string>

extern "C" JNIEXPORT jstring JNICALL
Java_io_cybears_rev_passwordchecker_MainActivity_stringFromJNI(
        JNIEnv* env,
        jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}

extern "C" JNIEXPORT jstring JNICALL
Java_io_cybears_rev_passwordchecker_MainActivity_checkPassword(
        JNIEnv* env,
        jobject /* this */, jstring password) {

    const char *str = env->GetStringUTFChars(password, 0);

    if (strcmp(str, "password")==0)
    {
        std::string true_str = "TRUE";
        return env->NewStringUTF(true_str.c_str());
    }

    std::string false_str = "FALSE";
    return env->NewStringUTF(false_str.c_str());

}


extern "C" JNIEXPORT jstring JNICALL
Java_io_cybears_rev_passwordchecker_MainActivity_checkPassword2(
        JNIEnv* env,
        jobject /* this */, jstring password) {

    const char *x = env->GetStringUTFChars(password, 0);

    int result = 1;
    int password_length = 0;

    password_length = strlen(x);

    if (password_length != 23)
    {
        //printf("Incorrect password length\n");
        std::string false_str = "FALSE";
        return env->NewStringUTF(false_str.c_str());
    }

    if (!(x[11]*x[3]-x[4]*x[17]==-1511))
    {
        result = -1;
    }
    if (!(x[9]-x[5]*x[14]+x[16]==-4992))
    {
        result = -1;
    }
    if (!(x[21]-x[22]*x[6]==-14312))
    {
        result = -1;
    }
    if (!(x[16]*x[21]*x[4]-x[5]==323769))
    {
        result = -1;
    }
    if (!(x[11]-x[15]*x[4]*x[6]==-546512))
    {
        result = -1;
    }
    if (!(x[13]-x[16]-x[1]-x[13]==-174))
    {
        result = -1;
    }
    if (!(x[0]-x[19]*x[17]*x[6]==-598131))
    {
        result = -1;
    }
    if (!(x[15]+x[11]*x[14]+x[8]==3866))
    {
        result = -1;
    }
    if (!(x[19]*x[21]*x[5]==366282))
    {
        result = -1;
    }
    if (!(x[1]+x[14]-x[4]-x[14]==24))
    {
        result = -1;
    }
    if (!(x[10]-x[8]-x[22]==-112))
    {
        result = -1;
    }
    if (!(x[1]*x[1]-x[18]==14592))
    {
        result = -1;
    }
    if (!(x[4]-x[15]*x[17]-x[19]==-4952))
    {
        result = -1;
    }
    if (!(x[14]*x[12]*x[21]==184275))
    {
        result = -1;
    }
    if (!(x[19]*x[22]-x[9]==6290))
    {
        result = -1;
    }
    if (!(x[7]*x[16]+x[8]==6601))
    {
        result = -1;
    }
    if (!(x[3]*x[1]+x[13]*x[10]==20201))
    {
        result = -1;
    }
    if (!(x[6]+x[11]+x[21]==261))
    {
        result = -1;
    }
    if (!(x[20]+x[12]-x[16]==112))
    {
        result = -1;
    }
    if (!(x[14]*x[21]-x[10]*x[10]==-6190))
    {
        result = -1;
    }
    if (!(x[0]*x[17]+x[6]==10213))
    {
        result = -1;
    }
    if (!(x[9]-x[16]-x[14]-x[3]==-114))
    {
        result = -1;
    }
    if (!(x[18]*x[11]+x[14]+x[6]==4227))
    {
        result = -1;
    }
    if (!(x[6]*x[15]-x[21]==5572))
    {
        result = -1;
    }
    if (!(x[14]+x[1]-x[2]+x[14]==113))
    {
        result = -1;
    }
    if (!(x[9]*x[16]-x[15]+x[11]==4539))
    {
        result = -1;
    }
    if (!(x[1]*x[10]*x[16]==609235))
    {
        result = -1;
    }
    if (!(x[6]-x[6]-x[5]+x[12]==-49))
    {
        result = -1;
    }
    if (!(x[13]-x[19]*x[13]==-4200))
    {
        result = -1;
    }
    if (!(x[14]+x[19]-x[9]-x[19]==-40))
    {
        result = -1;
    }
    if (!(x[21]*x[11]*x[1]-x[20]==632609))
    {
        result = -1;
    }
    if (!(x[22]+x[5]+x[18]==288))
    {
        result = -1;
    }
    if (!(x[20]-x[4]+x[17]+x[22]==230))
    {
        result = -1;
    }
    if (!(x[7]+x[11]-x[11]+x[11]==206))
    {
        result = -1;
    }
    if (!(x[19]*x[8]+x[14]+x[11]==4310))
    {
        result = -1;
    }
    if (!(x[4]*x[21]*x[1]+x[10]==739526))
    {
        result = -1;
    }
    if (!(x[5]+x[3]-x[18]-x[3]==65))
    {
        result = -1;
    }
    if (!(x[6]*x[4]+x[5]==11269))
    {
        result = -1;
    }
    if (!(x[14]*x[14]*x[17]-x[16]==206497))
    {
        result = -1;
    }
    if (!(x[4]-x[3]+x[22]+x[1]==242))
    {
        result = -1;
    }
    if (!(x[18]*x[21]+x[5]-x[13]==3117))
    {
        result = -1;
    }
    if (!(x[11]+x[8]*x[11]==6889))
    {
        result = -1;
    }
    if (!(x[8]-x[16]-x[11]+x[8]==28))
    {
        result = -1;
    }
    if (!(x[8]-x[12]*x[11]-x[12]==-5378))
    {
        result = -1;
    }
    if (!(x[18]+x[10]*x[11]*x[11]==654504))
    {
        result = -1;
    }
    if (!(x[16]-x[16]*x[12]*x[3]==-347892))
    {
        result = -1;
    }

    if (result == 1)
    {
        //printf("CONGRATULATIONS!\n");
        std::string true_str = "TRUE";
        return env->NewStringUTF(true_str.c_str());
    }

    if (result == -1)
    {
        //printf("Incorrect password\n");
        std::string false_str = "FALSE";
        return env->NewStringUTF(false_str.c_str());
    }

    std::string false_str = "FALSE";
    return env->NewStringUTF(false_str.c_str());

}


extern "C" JNIEXPORT jstring JNICALL
Java_io_cybears_rev_passwordchecker_MainActivity_checkPassword3(
        JNIEnv* env,
        jobject /* this */, jstring password) {

    const char *x = env->GetStringUTFChars(password, 0);

    int result = 1;
    int password_length = 0;

    password_length = strlen(x);

    if (password_length != 23)
    {
        //printf("Incorrect password length\n");
        std::string false_str = "FALSE";
        return env->NewStringUTF(false_str.c_str());
    }

    if (!(x[11]*x[3]-x[4]*x[17]==-1511))
    {
        result = -1;
    }
    if (!(x[9]-x[5]*x[14]+x[16]==-4992))
    {
        result = -1;
    }
    if (!(x[21]-x[22]*x[6]==-14312))
    {
        result = -1;
    }
    if (!(x[16]*x[21]*x[4]-x[5]==323769))
    {
        result = -1;
    }
    if (!(x[11]-x[15]*x[4]*x[6]==-546512))
    {
        result = -1;
    }
    if (!(x[13]-x[16]-x[1]-x[13]==-174))
    {
        result = -1;
    }
    if (!(x[0]-x[19]*x[17]*x[6]==-598131))
    {
        result = -1;
    }
    if (!(x[15]+x[11]*x[14]+x[8]==3866))
    {
        result = -1;
    }
    if (!(x[19]*x[21]*x[5]==366282))
    {
        result = -1;
    }
    if (!(x[1]+x[14]-x[4]-x[14]==24))
    {
        result = -1;
    }
    if (!(x[10]-x[8]-x[22]==-112))
    {
        result = -1;
    }
    if (!(x[1]*x[1]-x[18]==14592))
    {
        result = -1;
    }
    if (!(x[4]-x[15]*x[17]-x[19]==-4952))
    {
        result = -1;
    }
    if (!(x[14]*x[12]*x[21]==184275))
    {
        result = -1;
    }
    if (!(x[19]*x[22]-x[9]==6290))
    {
        result = -1;
    }
    if (!(x[7]*x[16]+x[8]==6601))
    {
        result = -1;
    }
    if (!(x[3]*x[1]+x[13]*x[10]==20201))
    {
        result = -1;
    }
    if (!(x[6]+x[11]+x[21]==261))
    {
        result = -1;
    }
    if (!(x[20]+x[12]-x[16]==112))
    {
        result = -1;
    }
    if (!(x[14]*x[21]-x[10]*x[10]==-6190))
    {
        result = -1;
    }
    if (!(x[0]*x[17]+x[6]==10213))
    {
        result = -1;
    }
    if (!(x[9]-x[16]-x[14]-x[3]==-114))
    {
        result = -1;
    }

    if (result == 1)
    {
        //printf("CONGRATULATIONS!\n");
        std::string true_str = "TRUE";
        return env->NewStringUTF(true_str.c_str());
    }

    if (result == -1)
    {
        //printf("Incorrect password\n");
        std::string false_str = "FALSE";
        return env->NewStringUTF(false_str.c_str());
    }

    std::string false_str = "FALSE";
    return env->NewStringUTF(false_str.c_str());

}

