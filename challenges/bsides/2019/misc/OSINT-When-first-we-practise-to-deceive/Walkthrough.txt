OSINT Challenge 

Open PCAP

Find EMAIL Address = AlphaUrsaeMajoris@cybears.tech

There are two paths you can take from here PATH A (DNS enumeration) and PATH B (Username enumeration)

PATH A (DNS enumeration)

Looking up the domain get interesting TXT record:

#Command
dig TXT cybears.tech
cybears.tech.		300	IN	TXT	"If you need to find me, email: AlphaUrsaeMajoris@cybears.site or visit my website."

Looking up the domain cybears.site
Will not resolve, or get a historical IP that is not valid.
There is nothing that interesting here except that "cybears.site" is a legit domain, just not currently in use, the previous TXT record also hints to a website.

(HARD PART possibly depending on the wordlist you use)
From here you can enumerate sub-domains looking for a valid website.

Note: The following DNS wordlists in SECLISTS contain the correct word (obscure.cybears.site)
However 'old' and 'archive' will also return the correct IP and are in most wordlists.

bitquark-subdomains-top100K.txt
jhaddix-dns.txt
sortedcombied-knock-dnsrecon-fierce-reconng.txt

#Command
dnsenum cybears.site -f /opt/SecLists/Discovery/DNS/subdomains-top1mil-5000.txt

...
Brute forcing with /opt/SecLists/Discovery/DNS/subdomains-top1mil-5000.txt:
____________________________________________________________________________

old.cybears.site.                        206      IN    A        13.211.3.235
archive.cybears.site.                    176      IN    A        13.211.3.235

Browsing to either of those sites will present the user with an image displaying:
Come in, We're Closed.

However the the virtual redirect on the website will change the URL to 'obscure.cybears.site'

From there you can put obscure.cybears.site into the wayback machine: https://web.archive.org/web/*/obscure.cybears.site
Where there is a snapshot of a wordpress site from Oct 14 2018.

Under the archived page "https://obscure.cybears.site/?p=44", there is a download that was not archived, however there is an MD5 hash with the file.

Putting this MD5 into Virustotal returns a hit.
https://www.virustotal.com/#/file/da756415192d278afb64e3c2c18d18334a90e0afc5512df50a01f8f3a8ca7b80/detection

There is a comment on this detection by user DecepticomTSS that links to a virustotal graph which links the Sample with the same MD5 hash to another submission.

https://www.virustotal.com/graph/gffdd9232a3d64309897039a324e9020350b5bfb9aa3a4730b60d564f0453ab97

In this graph there is a link to another detection where the user AlphaUM has commented the flag:

cybears{0Sint_Skillz_2_P@y_Th3_Bi11z}


PATH B (Username enumeration)

The username AlphaUrsaeMajoris is unique enough to try and find other accounts using the same name using a site or tool, such as 'https://checkusernames.com'.

One of the first valid accounts is a reddit account:
https://www.reddit.com/user/AlphaUrsaeMajoris

This user has only one post user the subreddit: https://www.reddit.com/r/MoHoneyMoProblems/

There are three moderators for this subreddit:
https://www.reddit.com/user/CyBarBearian
https://www.reddit.com/user/BearBytesMan
https://www.reddit.com/user/cykumamon

With all three having Twitter accounts connected to their accounts.
https://twitter.com/CBearian
https://twitter.com/BytesMan
https://twitter.com/cykumamon

If you analyze the users that the three profiles follow they have 4 in common between all three of them.

This can be done easily via several web apps.
e.g. "https://followerwonk.com/compare/CBearian/BytesMan/cykumamon"

Four profiles in common:
https://twitter.com/BSidesCbr
https://twitter.com/kylieengineer
https://twitter.com/silviocesare
https://twitter.com/Grizz77952942

Looking at "Grizz77952942" it lists a LinkedIn Account

On the LinkedIn account there is personal blog listed as: "obscure.cybears.site".

Browsing to "obscure.cybears.site" will present the user with an image displaying:
Come in, We're Closed.

From there you can put obscure.cybears.site into the wayback machine: https://web.archive.org/web/*/obscure.cybears.site
Where there is a snapshot of a wordpress site from Oct 14 2018.

Under the archived page "https://obscure.cybears.site/?p=44", there is a download that was not archived, however there is an MD5 hash with the file.

Putting this MD5 into Virustotal returns a hit.
https://www.virustotal.com/#/file/da756415192d278afb64e3c2c18d18334a90e0afc5512df50a01f8f3a8ca7b80/detection

There is a comment on this detection by user DecepticomTSS that links to a virustotal graph which links the Sample with the same MD5 hash to another submission.

https://www.virustotal.com/graph/gffdd9232a3d64309897039a324e9020350b5bfb9aa3a4730b60d564f0453ab97

In this graph there is a link to another detection where the user AlphaUM has commented the flag:

cybears{0Sint_Skillz_2_P@y_Th3_Bi11z}

