#!/bin/sh
docker run --name dind --privileged -d --rm docker:dind
docker cp level1a dind:/
docker cp level1b dind:/
docker cp solution.sh dind:/
docker cp flagxor.py dind:/
docker exec -it dind /solution.sh
docker stop dind
