#!/usr/bin/env python3
# pip install pyelftools lief
import os
import subprocess
import struct
import lief
from elftools.elf.elffile import ELFFile
import math
import nacl
import nacl.signing
import nacl.hash
import binascii

def list_to_bytes(l):
    return bytes(bytearray(l))

def bytes_to_list(b):
    return list(b)

def validate_signature(input_elf, public_key):
    e = lief.parse(input_elf)
    main_addr = struct.pack('L', e.get_symbol('main').value)
    executable_data = list_to_bytes(e.get_section('.text').content)

    with open("verify_dump", "wb") as d:
        d.write(executable_data)
    print("Segment hash: {}".format(binascii.b2a_hex(nacl.hash.sha256(executable_data, encoder=nacl.encoding.RawEncoder))))
    print("Segment size: {}".format(len(executable_data)))
    signature_section = e.get_section('.text.code_signature')
    signature_data = list_to_bytes(signature_section.content)

    signed_global_hash = signature_data[:64]
    signed_entry_hash = signature_data[64:128]
    signature_data = signature_data[128:]

    num_buckets = struct.unpack('i', signature_data[:struct.calcsize('i')])[0]
    print("Num buckets: {}".format(num_buckets))
    signature_data = signature_data[struct.calcsize('i'):]
    signature_data = signature_data[:num_buckets*32]

    hash_buckets = signature_data
    print("Layout:\n\tOffset: {}\n\tHash: {} bytes\n\tEntry Hash: {} bytes\n\tNum buckets: {} bytes\n\tbuckets: {} bytes".format(hex(signature_section.offset), 64, 64, struct.calcsize('i'), len(hash_buckets)))
    combined_hash = nacl.hash.sha256(hash_buckets, encoder=nacl.encoding.RawEncoder)
    print("Bucket hash: {}".format(binascii.b2a_hex(combined_hash)))
    entry_hash = nacl.hash.sha256(main_addr, encoder=nacl.encoding.RawEncoder)
    print("Entry: 0x{:x}".format(struct.unpack('L', main_addr)[0]))
    print("Entry hash:  {}".format(binascii.b2a_hex(entry_hash)))
    public_key.verify(entry_hash, signed_entry_hash) 
    public_key.verify(combined_hash, signed_global_hash) 


    remaining_buckets = hash_buckets
    remaining_text = executable_data
    buckets = []
    while len(remaining_buckets) > 0:
        buckets.append((remaining_buckets[:32], remaining_text[:0x4000]))
        remaining_buckets = remaining_buckets[32:]
        remaining_text = remaining_text[0x4000:]
    if len(remaining_text) > 0:
        raise Exception("Unaccounted for bytes in the .text section")
    if len(remaining_buckets) > 0:
        raise Exception("Unaccounted for hashes in the code signature")

    index = 0
    for hash, text in buckets:
        print("Bucket {}:\t{}".format(index, binascii.b2a_hex(hash)))
        if index > num_buckets:
            raise Exception("Incorrect number of buckets!")
        actual = nacl.hash.sha256(text, encoder=nacl.encoding.RawEncoder)
        if hash != actual:
            raise Exception("Hash mismatch! Bucket {}. Actual: {}".format(index, binascii.b2a_hex(actual)))
        index += 1

    print("Signature is valid")

def generate_key():
    private_key = nacl.signing.SigningKey.generate()
    public_key = private_key.verify_key
    return (private_key, public_key)

def load_key():
    if os.path.isfile('key'):
        with open("key", "rb") as f:
            private_key = nacl.signing.SigningKey(f.read())
            public_key = private_key.verify_key
    else:
        with open('key.pub', "rb") as f:
            print("[!] WARNING: Cannot load private key! Only verification is available")
            print("Generate a new key to perform signing operations.")
            private_key = None
            public_key = nacl.signing.VerifyKey(f.read())
    return (private_key, public_key)

def get_executable_segments(elf_file):
    segments = []
    for segment in elf_file.iter_segments():
        if 1 & segment['p_flags']:
            segments.append(segment)
    return segments

def sign_elf(input_elf, output_elf, signing_key):
    e = lief.parse(input_elf)
    main_addr = struct.pack('L', e.get_symbol('main').value)
    executable_data = list_to_bytes(e.get_section('.text').content)
    print("Segment hash: {}".format(binascii.b2a_hex(nacl.hash.sha256(executable_data, encoder=nacl.encoding.RawEncoder))))
    print("Segment size: {}".format(len(executable_data)))
    with open("sign_dump", "wb") as d:
        d.write(executable_data)

    expected_num_hashes = math.ceil(len(executable_data) / 0x4000)
    print("Expected number of hash buckets: {}".format(expected_num_hashes))
    sig_section_size = struct.calcsize('i') # number of buckets
    sig_section_size += 64 # signed hash of all buckets
    sig_section_size += 64 # signed hash of the entry point
    sig_section_size += 32 * expected_num_hashes
    print("Expected signature block size: {}".format(sig_section_size))

    # Construct the hash buckets
    remaining = executable_data
    buckets = list()
    while len(remaining) > 0:
        # Grab a page and hash it, then add that to the bucket
        data = remaining[:0x4000]
        remaining = remaining[0x4000:]
        hashed_data = nacl.hash.sha256(data, encoder=nacl.encoding.RawEncoder)
        buckets.append((hashed_data, data))

    # Hash all the buckets together
    combined_hash = b''
    for hash, data in buckets:
        combined_hash += bytes(hash)
    print("Hashing {} bytes of buckets".format(len(combined_hash)))
    bucket_hash = nacl.hash.sha256(combined_hash, encoder=nacl.encoding.RawEncoder)

    main_addr_hash = nacl.hash.sha256(main_addr, encoder=nacl.encoding.RawEncoder)

    # Sign the combined hash
    signed_hash = signing_key.sign(bucket_hash).signature
    signed_main_addr_hash = signing_key.sign(main_addr_hash).signature
    print("Signed hash:\t{}".format(binascii.b2a_hex(signed_hash)))
    print("Entry hash:\t{}".format(binascii.b2a_hex(main_addr_hash)))
    print("Bucket hash:\t{}".format(binascii.b2a_hex(bucket_hash)))

    assert len(signed_hash) == 64
    pack_string = '128s'
    signature_block = struct.pack(pack_string, signed_hash + signed_main_addr_hash)
    pack_string += 'i'
    signature_block += struct.pack('i', len(buckets))

    print("Buckets begin {} bytes into the signature block".format(len(signature_block)))

    index = 0
    for hash, data in buckets:
        assert 32 == len(hash)
        curr_pack_string = '32s'
        signature_block += struct.pack(curr_pack_string, hash)
        pack_string += curr_pack_string
        index += 1
        print("Bucket {} covers 0x{:x} bytes:\t{}".format(index, len(data), binascii.b2a_hex(hash)))
    print("Generated {} hash buckets, each hash is {} bytes".format(index, len(buckets[0][0])))
    print("Signature length: {}".format(len(signed_hash)))
    print("Pack string: {}".format(pack_string))
    with open(output_elf + ".sig_section", "wb") as f:
        f.write(signature_block)
    
    sig_section = lief.ELF.Section()
    sig_section.content = bytes_to_list(signature_block)
    sig_section.name = '.text.code_signature'
    e.add(sig_section)
    e.write(output_elf)

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser('ELF code signer')
    subs = parser.add_subparsers(dest='command')
    subs.add_parser('generate')
    sign = subs.add_parser('sign')
    sign.add_argument('INPUT')
    sign.add_argument('OUTPUT')
    verify = subs.add_parser('verify')
    verify.add_argument('INPUT')

    args = parser.parse_args()

    if args.command == 'generate':
        key = generate_key()
        with open("key", "wb") as f:
            f.write(key[0].encode(encoder=nacl.encoding.RawEncoder))
        with open("key.pub", "wb") as f:
            f.write(key[1].encode(encoder=nacl.encoding.RawEncoder))
    else:
        key = load_key()

    if args.command == 'verify':
        validate_signature(args.INPUT, key[1])
    elif args.command == 'sign':
        sign_elf(args.INPUT, args.OUTPUT, key[0])

